## Guess My Birthday Game ##

from random import randint

name = input("Hi! What is your name?  ")


for guess in range(5):
  month = randint(1, 12)
  year = randint(1924, 2004)
  print("Guess " + str(guess+1) + ": " + name + " were you born in "+
    str(month) + "/" + str(year) + "?")
  answer = input("yes or no?  ")
  if answer == "yes":
    print("I knew it!")
    exit()
  elif answer == "no" and guess == 4:
    print("I have other things to do. Good bye.")
    exit()
  elif answer == "no":
    print("Drat! Lemme try again!")
  else:
    print("Incorrect answer format. Please provide a yes or no.")
